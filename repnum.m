function [ xnew, nam] = repnum( rep)
%[ xnew, nam] = repnum( rep)
% Transforms the replicate information in 'rep' (normally a matrix) into
% one vector
%
%INPUT:
% rep   Matrix/ vector indicating replicates (all rows are identical)
%
%OUTPUT:
% xnew  Vector with equal number for replicates. Is automatically in
%        increasing order starting with 1
% nam   Name or number of replicate number

% 230901 AAR Added name as an output (for clarity)
%091012 AAR

[i, j] = fjernlike( rep);
if size( i, 1) == 1
    disp( 'There is only one sample!')
    xnew = ones( size( rep, 1), 1);
    return
end
xnew = vec( (1:size(i, 1))' * ones( 1, size(j,2)));
j = vec(j);
xnew( isnan(j)) = [];
j( isnan(j)) = [];
xnew(j)=xnew;
nam = i;