# README #

This folder holds generic m-functions that is used by a lot of the other m-functions in the other folders that exist here in my collection of folders.

Currently there are four functions:

- fjernlike: finds the unique inputs, and their locations

- findpart: find the rows/ strings that have a given text string included

- makeint: makes a vector of a given length with different numbers. In other words: creates segments/ intervals for cross-validation and/ or interval PLS

- repnum: creates running numbers for replicates. The input can be both cellstr, char and numbers

- vec: simply vectorizes the data columnwise


Debugging:
- If you encounter any errors, please contact me directly on: asmundrinnan@gmail.com with a screen dump of the error that you got