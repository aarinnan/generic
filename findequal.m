function [ind,code]=findequal(x)
%[ind,code]=findequal(x)
%
% Finds equal rows.
% Returns a matrix ind, where the equal rows are found rowwise.
% 
% INPUT
%  x    x-matrix
%
% OUTPUT
%  ind  Rowindexes for the equal rows
%  code Code from 1 to the number of unique rows

% Copyright, 2004 - 
% This M-file and the code in it belongs to the holder of the
% copyrights and is made public under the following constraints:
% It must not be changed or modified and code cannot be added.
% The file must be regarded as read-only. 
%
% �smund Rinnan
% Quality and Technology
% Department of Food Science
% Faculty of Life Sciences
% University of Copenhagen
% Rolighedsvej 30, 1958 Frederiksberg C, Denmark
% Phone:  +45 35 33 35 42
% e-mail: aar@life.ku.dk

if iscell(x)
    error('This function does not handle cells')
end
[r,k]=size(x);
[xr,in]=sortrows(x,1:k);
if ischar(x)
    uni=fjernlike(x);
    for i=1:size(uni,1)
        indtemp=find(strcmp(char(cellstr(uni(i,:))),cellstr(x))==1);
        ind(i,1:length(indtemp))=indtemp';
    end
    ind(ind==0)=NaN;
else
    if k==1
        xnew=[1;abs(xr(1:end-1,:)-xr(2:end,:))];
    else
        xnew=[1;sum(abs(xr(1:end-1,:)-xr(2:end,:))')];
    end
    a=find(xnew~=0);
    l=zeros(length(a),r);
    for i=1:length(a)
        b=find(x==xr(a(i)));
        l(i,1:length(b))=b';
    end
    l=l(sum(l')~=0,sum(l)~=0);
    l(l==0)=NaN;
    ind=l;
end
ind=sortrows(ind,1);
code=ones(size(ind,2),1)*(1:size(ind,1));
code(isnan(ind))=0;
code=vec(code);
code=code(code>0);