function ind = makeint( intnum, varnum)
%ind = makeint( intnum, varnum)
% Make an index for use with iPLS/ GA-PLS or iECVA with a specified number
%  of intervals for a given number of variables
%
%INPUT:
% intnum  Number of intervals to divide the variables into
% varnum  Number of variables in the data
%
%OUTPUT:
% ind     A vector with the length equalling the number of variables with
%          equal numbers for the different intervals

% AAR 130111

    %Divide the intervals evenly in the range given. If the number of intervals
    %don't go up in an even number, the smaller sized intervals are evenly
    %distributed in the range

ind = ceil( varnum/ intnum);
ind = ones( ind, 1) * (1:intnum);
too = prod( size( ind) ) - varnum;
pos = floor( size( ind, 2) / too);
%Evenly distribute the uneven intervals in the data
pos = (0:pos:pos * (too-1)) + ceil( size( ind, 2) / (too * 2) );
if ~isempty( pos)
    ind( end, pos) = NaN;
end
ind = vec( ind);
ind( isnan( ind) ) = [];