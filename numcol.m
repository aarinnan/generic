function col=numcol(n)
%Makes a RGB-matrix with n different colors
% col=numcol(n)
%
%INPUT:
% n    Number of different colors
%
%OUTPUT:
% col  A RGB-matrix

% AAR 261007

col=[.9 0 0   % 1 - Red
    0 .9 0    % 2 - Green
    0 0 .9    % 3 - Blue
    .8 .8 0   % 4 - Yellow
    .8 0 1    % 5 - Magenta
    0 .7 1    % 6 - Cyan
    .6 .6 .6  % 7 - Light grey
    0 0 0     % 8 - Black
    1 .6 .6   % 9 - Peach
    .4 .6 .6  % 10 - Dirty cyan
    .6 .4 .1  % 11 - Brown
    1 .2 .6   % 12 - Pink
    .7 0 .9   % 13 - Lilac
    .2 1 .6   % 14 - Green-cyan
    .6 1 .2   % 15 - Green-yellow
    .5 .3 .7  % 16 - Dirty lilac
    1 .6 .2   % 17 - Orange
    .5 .5 1   % 18 - Magenta-Blue
    .2 .6 1]; % 19 - 
if n > size(col,1)
    %Use the colormaps which has been made by Matlab
    disp( 'Colors found by the use of Matlab''s ''colormap'' (jet)')
    col = colormap( jet);
    num = round( 1:63/(n-1):64);
    col = col( num, :);    
end
col=col(1:n,:);